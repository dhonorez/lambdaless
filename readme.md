# Serverless Hipster, try lambdaless!

The following repo shows the use of contract-first development with AWS, API Gateway and CloudFormation.

The examples implement an OpenAPI/Swagger contract that can be found on the root: `gamehaxx-swagger.yml`

## Step 1: Mocking

This step illustrates how to return mock data from an API Gateway endpoint.

Run the following commands:
```
make package-step01
make deploy
```

To demo this step, run the following command:
```
make invoke-get
```

## Step 2: Http invocation

This step passes API calls from API Gateway to another HTTP endpoint, in this case IGDB. You need to fill in your IGDB user key for this to work.

Run the following commands:
```
make package-step02
make deploy
```

To demo this step, run the following command:
```
make invoke-search
```

## Step 3: AWS Integration

This steps shows the 'lambdaless' magic. It integrates with another AWS service without the need for lambda. Fails intentionally to show the use of mappings.

Run the following commands:
```
make package-step03
make deploy
```

To demo this step, run the following command:
```
make invoke-get-game
make populate-db
```

## Step 4: AWS Integration

This steps shows the 'lambdaless' magic. It integrates with another AWS service without the need for lambda.

Run the following commands:
```
make package-step04
make deploy
```

To demo this step, run the following command:
```
make populate-db
make invoke-get-game
```

## Step 5: Canary release

This steps adds a canary deployment to your CloudFormation. Don't forget to fill in your previous deployment id. This can be found under CloudFormation -> Resources.

Run the following commands:
```
make package-step05
make deploy
```

To demo this step, run the following command a few times. You will see the output change with each request, according to the traffic percentage.
```
make invoke-get-game
```
