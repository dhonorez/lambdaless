stageUrl = https://pfgwu852g6.execute-api.eu-west-1.amazonaws.com/v1
search = zelda
game = 1

clean:
	rm -rf build/*

make profile-haxx:
	export AWS_PROFILE=haxx

bucket:
	aws s3 mb s3://gamehaxx --region eu-west-1

build-mocked:
	sam build --template step01-mock/cf-gamehaxx-mock.yml

package-step01:
	sam package --template-file step01/cf-gamehaxx-mock.yml --output-template-file build/packaged.yaml --s3-bucket gamehaxx
	
package-step02:
	sam package --template-file step02/cf-gamehaxx-http.yml --output-template-file build/packaged.yaml --s3-bucket gamehaxx

package-step03:
	sam package --template-file step03/cf-gamehaxx-aws.yml --output-template-file build/packaged.yaml --s3-bucket gamehaxx

package-step04:
	sam package --template-file step04/cf-gamehaxx-aws-mapping.yml --output-template-file build/packaged.yaml --s3-bucket gamehaxx

package-step05:
	sam package --template-file step05/cf-gamehaxx-lambda-canary.yml --output-template-file build/packaged.yaml --s3-bucket gamehaxx

deploy:
	aws cloudformation deploy --template-file build/packaged.yaml --stack-name gamehaxx --capabilities CAPABILITY_IAM

invoke-get:
	curl -H "Content-Type:application/json" $(stageUrl)/games

invoke-search:
	curl -H "Content-Type:application/json" $(stageUrl)/search?game=$(search)

populate-db:
	curl -H "Content-Type:application/json" -X PUT -d "{\"name\": \"The Legend of Zelda: Breath of the Wild\", \"publish_date\":\"03/03/2017\"}" $(stageUrl)/game/1
	curl -H "Content-Type:application/json" -X PUT -d "{\"name\": \"The Last of Us Remastered\", \"publish_date\":\"29/07/2014\"}" $(stageUrl)/game/2
	curl -H "Content-Type:application/json" -X PUT -d "{\"name\": \"Uncharted 4: A Thief's End\", \"publish_date\":\"10/05/2016\"}" $(stageUrl)/game/3

invoke-get-game:
	curl -H "Content-Type:application/json" $(stageUrl)/game/$(game)

invoke-canary:
	curl -H "Content-Type:application/json" $(stageUrl)/game/1
